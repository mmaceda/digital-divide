import {Map, View} from 'ol';
import {fromLonLat} from "ol/proj";
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import KML from "ol/format";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import 'ol/ol.css';
import KML from 'ol/format/KML';
import Map from 'ol/Map';
import Stamen from 'ol/source/Stamen';
import VectorSource from 'ol/source/Vector';
import View from 'ol/View';
import {Fill, Stroke, Style} from 'ol/style';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';

class InternetResult {
  constructor(zipcode, withInternet, withoutInternet, dialUpOnly, cellOnly, satellite) {
    this.zipcode = zipcode;
    this.withInternet = Number(withInternet);
    this.withoutInternet = Number(withoutInternet);
    this.total = this.withInternet + this.withoutInternet;
    this.dialupOnly = Number(dialUpOnly);
    this.cellOnly = Number(cellOnly);
    this.satellite = Number(satellite);
    this.ratio = (this.withoutInternet + this.dialupOnly + this.cellOnly + this.satellite) / this.total * 100;


  }
}
var d = []

const internetData = 'https://api.airtable.com/v0/appEjxpGdZbpP6fx4/data?fields%5B%5D=ZipCode&fields%5B%5D=GEO_ID&fields%5B%5D=S2801_C01_012E&fields%5B%5D=S2801_C01_013E&fields%5B%5D=S2801_C01_014E&fields%5B%5D=S2801_C01_015E&fields%5B%5D=S2801_C01_016E&fields%5B%5D=S2801_C01_017E&fields%5B%5D=S2801_C01_018E&fields%5B%5D=S2801_C01_019E&api_key=keyX37wBdUbWRwfzL'

const build_maps = function(details) {
    let layers = []

    // Main map layer goes first
    layers.push(new TileLayer({
        source: new OSM(),
    }));


    // Now overlay the KML files
    for (let i in details) {
    layers.push(
        new VectorLayer({
          source: new VectorSource({
            url: 'kml/zip'+ details[i].zipcode +'.kml',
            format: new KML({
              extractStyles: false
            }),
          }),
          style: styleFunction
        })
    );
    }

const map = new Map({
  target: 'map',
  layers: layers,
  view: new View({
    center: fromLonLat([-95.36327, 29.76328]),
    zoom: 11
  })
});

}




async function get_data() {
  let results = [];
  const response = await fetch(internetData).then( response => {
    return response.json()
  }).then( results => {
    let final = []
    for (const key in results.records) {
          const r = results.records[key]['fields']
          //Limiting results right now for hitting AWS bucket
          var limit = []
          //var limit = ['77002', '77003', '77004', '77005', '77006', '77577', '77560']
          if (!limit.includes(r['ZipCode'])) {
            final.push(
                // S2801_C01_012E - total with internet
                // S2801_C01_019E - total without internet
                // S2801_C01_013E - Dial-up with no other type of Internet subscription
                // S2801_C01_016E - Cellular data plan with no other type of Internet subscription
                // S2801_C01_018E - Satellite Internet service -
                new InternetResult(r['ZipCode'], r['S2801_C01_012E'], r['S2801_C01_019E'], r['S2801_C01_013E'], r['S2801_C01_016E'], r['S2801_C01_018E'])
            )
          }
        }
    return final;
  }).then(final => {
      d = final;
      build_maps(final);
  }).catch( error => {
    console.error('error, error!', error);
  });

}






const styleFunction = function (feature) {
    const zipcode = feature.get('ZCTA5CE10');
    const ratio = d.filter(area => area.zipcode == feature.get('ZCTA5CE10'))[0].ratio;
    const opacity = ratio / 100


    return new Style({
    fill: new Fill({
      color: [0xff, 0x00, 0x00, opacity],
    }),
    stroke: new Stroke({
      color: '#000000',
    }),
  });
};




/// Run everything
get_data();

